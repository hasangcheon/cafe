class AnswersController < ApplicationController
	def create
		@answer = current_user.answers.build(answer_params)
		@answer.save

		redirect_to @answer.question
	end

	private
    # Never trust parameters from the scary internet, only allow the white list through.
    def answer_params
      params.require(:answer).permit(:content, :user_id, :question_id)
    end
end
